#=============================================================================
#                         -- SVILUPPATORE PROGETTO --
#=============================================================================
#
#      - Luca Samuele Vanzo (735924)
#
#=============================================================================
#                   -- MATRIX - PROGRAMMAZIONE CPP 2013 --
#=============================================================================
#
#      - Specifiche -
#
#      Il progetto richiede la progettazione e realizzazione di una 
#      classe generica che implementa una Matrice di elementi di tipo T. 
#      Oltre ai normali metodi fondamentali e tipici di una classe 
#      container (es. getValue, setValue, getRows, ecc...), devono 
#      essere soddisfatti i seguenti vincoli implementativi:
#
#      1. Deve essere possibile costruire una Matrice di elementi 
#      di tipo T da una Matrice di elementi di tipo Q. Lasciate al
#      compilatore la gestione della convertibilità di tipo Q a tipo T.
#
#      2. La classe deve includere il supporto agli iteratori. 
#      Nel caso della Matrice, l’iteratore deve scorrere gli elementi 
#      in sequenza dall’alto al basso e da destra verso sinistra.
#
#      3. L’accesso in lettura e scrittura agli elementi della Matrice 
#      deve essere possibile anche usando la seguente notazione:
#          # M1 e M2 sono due istanze di matrici
#          M1(0,0) = M2(10,7);
#
#      4. Deve essere possibile stampare (a monitor) il contenuto 
#      della Matrice mediante l’operatore di stream “<<”.
#
#      5. Implementare una funzione globale verify che, date due Matrici 
#      (es. M1 e M2) di elementi di tipo T e un predicato binario P 
#      (cioè che prende due parametri), ritorna true se e solo se:
#          P( M1(i,j), M2(i,j) ) == true            (∀ i, j)
#
#      Ad esempio, P(x,y) potrebbe corrispondere a: “elementi identici,
#      x==y”, “x pari e y dispari”, “y è una sottostringa di x”, ecc...
#
#      Utilizzare dove opportuno la gestione delle eccezioni.
#
#=============================================================================
#      NOME PROGRAMMA IN OUTPUT
#=============================================================================

PROGRAM_NAME    = main.exe

#=============================================================================
#      FLAG DI COMPILAZIONE
#=============================================================================

CC              = g++
ODIR            = obj
SDIR            = src
DOXY            = doc

CXXFLAGS        = -Wall -DNDEBUG -O

#=============================================================================

main.exe: $(ODIR)/main.o
		  $(CC) $(CXXFLAGS) ./$(ODIR)/main.o -o $(PROGRAM_NAME)

$(ODIR)/main.o: ./$(SDIR)/main.cpp ./$(SDIR)/matrix.h
				$(CC) -c $(CXXFLAGS) ./$(SDIR)/main.cpp -o ./$(ODIR)/main.o

#=============================================================================

clean:  
		@rm -f -R ./$(DOXY)
		@rm -f -R ./$(DOXY)
		@rm -f -v ./$(ODIR)/*.o
		@rm -f -v ./$(PROGRAM_NAME)

#=============================================================================

run: 
		@$(MAKE) && ./$(PROGRAM_NAME)


#=============================================================================

docs: 
		@doxygen ./doxygen.conf

#=============================================================================
#                                   EOF
#=============================================================================
