//=============================================================================
//                         -- SVILUPPATORE PROGETTO --
//=============================================================================
//
//      - Luca Samuele Vanzo (735924)
//
//=============================================================================
//                   -- MATRIX - PROGRAMMAZIONE CPP 2013 --
//=============================================================================
//
//      - Specifiche -
//
//      Il progetto richiede la progettazione e realizzazione di una 
//      classe generica che implementa una Matrice di elementi di tipo T. 
//      Oltre ai normali metodi fondamentali e tipici di una classe 
//      container (es. getValue, setValue, getRows, ecc...), devono 
//      essere soddisfatti i seguenti vincoli implementativi:
//
//      1. Deve essere possibile costruire una Matrice di elementi 
//      di tipo T da una Matrice di elementi di tipo Q. Lasciate al
//      compilatore la gestione della convertibilità di tipo Q a tipo T.
//
//      2. La classe deve includere il supporto agli iteratori. 
//      Nel caso della Matrice, l’iteratore deve scorrere gli elementi 
//      in sequenza dall’alto al basso e da destra verso sinistra.
//
//      3. L’accesso in lettura e scrittura agli elementi della Matrice 
//      deve essere possibile anche usando la seguente notazione:
//          // M1 e M2 sono due istanze di matrici
//          M1(0,0) = M2(10,7);
//
//      4. Deve essere possibile stampare (a monitor) il contenuto 
//      della Matrice mediante l’operatore di stream “<<”.
//
//      5. Implementare una funzione globale verify che, date due Matrici 
//      (es. M1 e M2) di elementi di tipo T e un predicato binario P 
//      (cioè che prende due parametri), ritorna true se e solo se:
//          P( M1(i,j), M2(i,j) ) == true            (∀ i, j)
//
//      Ad esempio, P(x,y) potrebbe corrispondere a: “elementi identici,
//      x==y”, “x pari e y dispari”, “y è una sottostringa di x”, ecc...
//
//      Utilizzare dove opportuno la gestione delle eccezioni.
//
//=============================================================================

#ifndef MATRIX_H
#define MATRIX_H

//=============================================================================
//      LIBRERIE STANDARD
//=============================================================================

#include <cassert>                                          // assert
#include <iostream>                                         // std::ostream

//=============================================================================
//      MATRIX - CLASS HEADER
//=============================================================================

template <typename T>
class Matrice
{
private:

    //=========================================================================
    //      MATRIX - VARIABILI PRIVATE
    //=========================================================================

    unsigned int _rows;
    unsigned int _cols;
    T *_buffer;


public:

    typedef unsigned int index_type;
    typedef T value_type;

    //=========================================================================
    //      MATRIX - COSTRUTTORI
    //=========================================================================

    /**
     * Costruttore di Default
     */
    Matrice()
    {
        _rows      = 0;
        _cols      = 0;
        _buffer    = NULL;
        
        #ifndef NDEBUG
        std::cout << "[DEBUG] Matrice<T>::Matrice( )" << std::endl;  
        #endif
    }

    /**
     * Costruttore di Default
     * @param row Valore della coordinata riga
     * @param col Valore della coordinata colonna
     */
    Matrice(const index_type row, const index_type col)
    {
        _rows      = 0;
        _cols      = 0;
        _buffer    = NULL;
        
        try
        {
            resize(row, col);
        }
        catch(char *str)
        {    
            std::cout << str << std::endl; 
            throw str;
        }     
        
        #ifndef NDEBUG
        std::cout << "[DEBUG] Matrice<T>::Matrice( int, int )" << std::endl;  
        #endif
    }

    /**
     * Costruttore di Default
     * @param row Valore della coordinata riga
     * @param col Valore della coordinata colonna
     * @param val Valore di default delle celle
     */
    Matrice(const index_type row, const index_type col, const value_type &val)
    {
        _rows      = 0;
        _cols      = 0;
        _buffer    = NULL;

        try
        {
            resize(row, col, val);      
        }
        catch(char *str)
        {    
            std::cout << str << std::endl; 
            throw str;
        }
        
        #ifndef NDEBUG
        std::cout << "[DEBUG] Matrice<T>::Matrice( int, int, T )" << std::endl;  
        #endif
    }
    
    /**
     * Costruttore per Copia
     * @param other Matrice da copiare post resize
     */
    Matrice(const Matrice &other)
    {
        _rows      = 0;
        _cols      = 0;
        _buffer    = NULL;

        try
        {
            resize(other.getRows(), other.getCols());

            for(index_type i = 0; i < (_rows * _cols); i++)
                _buffer[i] = other.getIndex(i);
        }
        catch(char *str)
        {    
            std::cout << str << std::endl; 
            throw str;
        }
        
        #ifndef NDEBUG
        std::cout << "[DEBUG] Matrice<T>::Matrice( Matrice<T> )" << std::endl;  
        #endif
    }

    /**
     * Costruttore di Conversione.
     * Data una Matrice di tipo Q in input ottengo una Matrice di tipo T.
     * La conversione è possibile se Q è convertibili in T. 
     * La convertibilità viene gestita dal compilatore.
     * @param other Matrice da convertire in Matrice<T>( ... )
     */
    template <typename Q>   
    Matrice(const Matrice<Q> &other) 
    {
        _rows      = 0;
        _cols      = 0;
        _buffer    = NULL;
    
        try 
        {
            resize(other.getRows(), other.getCols());

            for(index_type i = 0; i < (other.getRows() * other.getCols()); ++i)
                _buffer[i] = static_cast<value_type>(other.getIndex(i)); 
        }
        catch(char *str)
        {    
            std::cout << str << std::endl; 
            throw str;
        }
        
        #ifndef NDEBUG
        std::cout << "[DEBUG] Matrice<T>::Matrice( Matrice<Q> )" << std::endl;  
        #endif
    }

    //=========================================================================
    //      MATRIX - DISTRUTTORE
    //=========================================================================

    /**
     * Distruttore di Default
     */
    ~Matrice()
    {
        if(_buffer != NULL)
            delete[] _buffer;

        _rows      = 0;
        _cols      = 0;
        _buffer    = NULL;
        
        #ifndef NDEBUG
        std::cout << "[DEBUG] Matrice<T>::~Matrice( )" << std::endl;  
        #endif
    }

    //=========================================================================
    //      MATRIX - METODI DI SUPPORTO
    //=========================================================================

    /**
     * Ritorna le dimensioni della Matrice: Righe e Colonne.
     * @return Dimensione della Matrice per Coordinate
     */
    const index_type getRows() const { return _rows; }
    const index_type getCols() const { return _cols; }

    /**
     * Permette di accedere ai valori mediante Indice
     * @param index Valore indice del vettore Matrice
     * @return Valore T della cella nella posizione N
     */
    const value_type getIndex(const index_type index) const 
    {
        if(index < 0)
            throw "[EXCEPTION] Index immessa minore di ZERO!\n[SOURCE IN] Matrice<T>::getIndex( ... )";

        if(index > (_rows * _cols))
            throw "[EXCEPTION] Index immessa fuori limite!\n[SOURCE IN] Matrice<T>::getIndex( ... )";
        
        #ifndef NDEBUG
        std::cout << "[DEBUG] Matrice<T>::GetIndex( int ) -> int" << std::endl;  
        #endif

        return _buffer[index];    
    }

    /**
     * Permette l'accesso a valori MATRICE come VETTORI.
     * ----------------------------------------------------
     * Struttura matrice logica: rows, cols.
     *
     *  +----+----+----+----+----+
     *  | 1  | 2  | 3  | 4  | 5  |
     *  +----+----+----+----+----+
     *  | 6  | 7  | 8  | 9  | 10 |
     *  +----+----+----+----+----+
     *  | 11 | 12 | 13 | 14 | 15 |
     *  +----+----+----+----+----+
     *  | 16 | 17 | 18 | 19 | 20 |
     *  +----+----+----+----+----+   Y
     *  | 21 | 22 | 23 | 24 | 25 |
     *  +----+----+----+----+----+
     *                               |
     *                      X     -- +
     *
     * Indicizzazione Lineare (Linear Indexing).
     * ----------------------------------------------------
     * Struttura matrice fisica: Index (vettore).
     *
     *  <--- +----+----+----+----+----+----+----+ --->
     *   ... | 10 | 11 | 12 | 13 | 14 | 15 | 16 | ...
     *  <--- +----+----+----+----+----+----+----+ --->
     *
     * Lo scopo è convertire una MATRICE in VETTORE
     * per rendere le celle accessibile tramite indice
     * e non più coordinate come per la matrice.
     * 
     * @param row Valore della coordinata riga
     * @param col Valore della coordinata colonna
     * @return Valore T della cella in posizione NxM
     */
    const value_type getValue(const index_type row, const index_type col) const 
    {
        try
        {
            if(row >= _rows || col >= _cols)
                throw "[EXCEPTION] Index immessa fuori limite!\n[SOURCE IN] Matrice<T>::getValue( ... )";

            if(row < 0 || col < 0)
                throw "[EXCEPTION] Index immessa minore di ZERO!\n[SOURCE IN] Matrice<T>::getValue( ... )";

            #ifndef NDEBUG
            std::cout << "[DEBUG] Matrice<T>::GetIndex( int, int ) -> T" << std::endl;  
            #endif

            return getIndex((row * _cols) + col); 
        }
        catch(char *str)
        {    
            std::cout << str << std::endl;
            throw str;
        }

        #ifndef NDEBUG
        std::cout << "[DEBUG] Matrice<T>::GetIndex( int, int ) -> T (EXCEPTION)" << std::endl;  
        #endif

        return static_cast<value_type>(0);
    }

    //=========================================================================
    //      MATRIX - METODI DI SCRITTURA
    //=========================================================================

    /**
     * Metodo matrice per la modifica dei valori T delle celle.
     * Date le coordinate viene calcolato il valore indice.
     * @param row Valore della coordinata riga
     * @param col Valore della coordinata colonna
     * @param val Valore di default
     * @return Valore T della cella nella posizione NxM
     */
    void setValue(const index_type row, const index_type col, const T &val)
    {
        try
        {
            if(row >= _rows || col >= _cols)
                throw "[EXCEPTION] Index immessa fuori limite!\n[SOURCE IN] Matrice<T>::setValue( ... )";

            if(row < 0 || col < 0)
                throw "[EXCEPTION] Index immessa minore di ZERO!\n[SOURCE IN] Matrice<T>::setValue( ... )";

            index_type index = (row * _cols) + col;
            _buffer[index] = val;

            #ifndef NDEBUG
            std::cout << "[DEBUG] Matrice<T>::SetValue( int, int, T )" << std::endl;  
            #endif
        }
        catch(char *str)
        {    
            std::cout << str << std::endl;
            throw str;
        }
    }

    //=========================================================================
    //      MATRIX - METODI DI RIDIMENSIONAMENTO
    //=========================================================================

    /**
     * Permette di ridimensionare la Matrice da NxM a N1xM1.
     * I valori immessi precedentemente saranno mantenuti.
     * @param row Valore della coordinata riga
     * @param col Valore della coordinata colonna
     */
    void resize(const index_type row, const index_type col)
    {
        value_type* _tmp_bfr    = _buffer;
        index_type _tmp_row     = _rows;
        index_type _tmp_col     = _cols;
        
        try
        {
            _rows               = row;
            _cols               = col;
            _buffer             = new value_type[(_rows * _cols)];

            for(index_type i = 0; i < (_rows * _cols); i++)
                _buffer[i] = T();

            if(_tmp_bfr != NULL)
            {
                for(index_type i = 0; i < _tmp_row; i++)
                    for(index_type n = 0; n < _tmp_col; n++)
                    {
                        index_type idxold = (i * _tmp_col) + n;
                        index_type idxnew = (i * _cols) + n;

                        if(idxnew >= (_rows * _cols))
                            break;

                        _buffer[idxnew] = _tmp_bfr[idxold];
                    }
            }

            delete[]            _tmp_bfr;
            _tmp_bfr            = NULL;
            
            #ifndef NDEBUG
            std::cout << "[DEBUG] Matrice<T>::Resize( int, int )" << std::endl;  
            #endif
        }
        catch(...)
        {
            _rows               = _tmp_row;
            _cols               = _tmp_col;
            _buffer             = _tmp_bfr;
            _tmp_bfr            = NULL;

            throw "[EXCEPTION] Errore di allocazione!\n[SOURCE IN] Matrice<T>::Resize( int, int )";
        }
    }

    /**
     * Permette di ridimensionare la Matrice da NxM a N1xM1.
     * I valori immessi precedentemente NON saranno mantenuti.
     * @param row Valore della coordinata riga
     * @param col Valore della coordinata colonna
     * @param val Valore di default delle celle
     */
    void resize(const index_type row, const index_type col, const value_type &val)
    {
        try
        {
            resize(row, col);
            
            for(index_type i = 0; i < (_rows * _cols); i++)
                _buffer[i] = val;

            #ifndef NDEBUG
            std::cout << "[DEBUG] Matrice<T>::Resize( int, int )" << std::endl;  
            #endif
        }
        catch(...)
        {
            throw "[EXCEPTION] Errore di allocazione!\n[SOURCE IN] Matrice<T>::Resize( int, int, T )";
        }
    }

    //=========================================================================
    //      MATRIX - METODI DI STAMPA
    //=========================================================================

    /**
     * Metodo di visualizzazione grafica (nella forma NxM) della Matrice
     */
    void show() const
    {
        std::cout << "----------------------------------------"
                  << "----------------------------------------" 
                  << std::endl
                  << "\tMatrice( " << _rows << ", " << _cols << " )"
                  << std::endl
                  << "----------------------------------------"
                  << "----------------------------------------" 
                  << std::endl;

        if(!_buffer || (_cols * _rows) <= 0)
            std::cout << "\t[ ... Empty ... ]" << std::endl;
        else 
            for(index_type i = 0; i < _rows; i++) 
            {
                for(index_type n = 0; n < _cols; n++) 
                    std::cout << "\t" << getValue(i, n);

                std::cout << std::endl;
            }

        std::cout << std::endl;
    }

    //=========================================================================
    //      MATRIX - OPERATORI MATRICE
    //=========================================================================

    /**
     * Operatore di Assegnamento 
     * @param other Matrice in input da copiare
     * @return Nuova matrice ridimensionata su NxM in input
     */
    Matrice& operator=(const Matrice &other)
    {
        try
        {
            if (this != &other)
            {
                resize(other.getRows(), other.getCols());

                for(index_type i = 0; i < other.getRows(); ++i)
                    for(index_type n = 0; n < other.getCols(); ++n)
                        setValue(i, n, other.getValue(i, n));
                
                #ifndef NDEBUG
                std::cout << "[DEBUG] Matrice<T>::Operator=( Matrice<T> ) -> Matrice<T>" << std::endl;  
                #endif
            }
        }
        catch(char *str)
        {    
            std::cout << str << std::endl;
            throw str;
        }

        return *this;
    }

    /**
     * Operatore Parentesi tonde (Sola Lettura)
     * @param row Valore della coordinata riga
     * @param col Valore della coordinata colonna
     * @return Valore T in coordinata NxM
     */
    const value_type operator()(const index_type row, const index_type col) const 
    {
        assert(row < _rows && col < _cols);
        assert(row > 0 && col > 0);

        #ifndef NDEBUG
        std::cout << "[DEBUG] Matrice<T>::Operator( )( int, int )" << std::endl;  
        #endif

        return getValue(row, col);
    }

    /**
     * Operatore Parentesi tonde (Lettura/Scrittura)
     * @param row Valore della coordinata riga
     * @param col Valore della coordinata colonna
     * @return Valore T in coordinata NxM
     */
    value_type& operator()(const index_type row, const index_type col)
    {
        assert(row < _rows && col < _cols);
        assert(row > 0 && col > 0);

        #ifndef NDEBUG
        std::cout << "[DEBUG] Matrice<T>::Operator( )( int, int ) -> T&" << std::endl;  
        #endif

        index_type index = (row * _cols) + col;
        return _buffer[index];
    }

    //=========================================================================
    //      MATRIX - ITERATORI CLASSE
    //=========================================================================

    class const_iterator;
    class iterator
    {    
        friend class const_iterator;  
        friend class Matrice;
                
        public:
            
            // Traits dell'iteratore
            // Servono per rendere usabile l'iteratore con la STL
            typedef std::forward_iterator_tag   iterator_category;      
            typedef T                           value_type;           
            typedef T*                          pointer;      
            typedef T&                          reference; 
                    
            iterator() : ptr(0) {}
                
            iterator(const iterator &other) : ptr(other.ptr) {}
            
            iterator& operator=(const iterator &other) 
            {
                ptr = other.ptr;
                return *this;
            }
            
            ~iterator() {}
            
            /**
             * Operatore di dereferenziamento.
             * @return il dato "puntato" dall'iteratore.
             */
            reference operator*() const 
            {
                return *ptr;
            }       
            
            /**
             * Operatore freccia. 
             * @return il puntatore al dato "puntato" dall'iteratore.
             */
            pointer operator->() const 
            {
                return ptr;
            }
        
            /**
             * Operatore di confronto (uguaglianza). 
             * @param other iteratore da confrontare
             * @return true se i due iteratori "puntano" allo stesso dato
             */
            bool operator==(const iterator &other) const 
            {
                return ptr == other.ptr;
            }
            
            /**
             * Operatore di confronto (disuguaglianza). 
             * @param other iteratore da confrontare
             * @return true se i due iteratori "puntano" a dati diversi
             */
            bool operator!=(const iterator &other) const 
            {
                return !(*this == other);
            }

            /**
             * Operatore di confronto (uguaglianza). 
             * @param other const_iteratore da confrontare
             * @return true se i due iteratori "puntano" allo stesso dato
             */
            bool operator==(const const_iterator &other) const
            {
                return ptr == other.ptr; // vedi const_iterator
            }
        
            /**
             * Operatore di confronto (uguaglianza). 
             * @param other const_iteratore da confrontare
             * @return true se i due iteratori "puntano" a dati diversi
             */
            bool operator!=(const const_iterator &other) const 
            {
                return !(*this == other);
            }

            /**
             * Operatore di "elemento successivo". 
             * Versione pre-incremento. 
             * @return l'iteratore che "punta" all'elemento successivo
             */
            iterator &operator++() 
            {
                ++ptr;
                return *this;
            } 
            
            /**
             * Operatore di "elemento successivo". 
             * Versione post-incremento. 
             * @return l'iteratore che "punta" all'elemento successivo
             */
            iterator operator++(int) 
            {
                iterator tmp(ptr);
                ++ptr;
                return tmp;
            }
        

        private:
                
            value_type *ptr;
            
            /**
             * Costruttore di inizializzazione. 
             * Sarà usato anche da Matrice.
             * @param p puntatore ai dati 
             */
            explicit iterator(value_type *p) : ptr(p) {}    

    };

    //=========================================================================
    //      MATRIX - CONST ITERATORI CLASSE
    //=========================================================================

    class const_iterator {
        
        // diamo a iterator e Matrice il permesso di accedere alla sezione 
        // private di const_iterator.
        friend class iterator;  
        friend class Matrice;
                
    public:
        
        // Traits dell'iteratore
        // Servono per rendere usabile l'iteratore con la STL
        typedef std::forward_iterator_tag   iterator_category;      
        typedef T                           value_type;  
        typedef const T*                    pointer;      
        typedef const T&                    reference; 

        const_iterator() : ptr(0) {}
        
        const_iterator(const const_iterator &other) : ptr(other.ptr) {}
        
        const_iterator& operator=(const const_iterator &other) 
        {
            ptr = other.ptr;
            return *this;
        }
        
        ~const_iterator() {}
        
        /**
         * Costruttore di conversione. 
         * No explicit perchè vogliamo permettere le conversioni di tipo implicite
         * @param other iterator da convertire
         */
        const_iterator(const iterator &other) : ptr(other.ptr) {}
        
        /**
         * Operatore di dereferenziamento. 
         * @return il dato "puntato" dall'iteratore.
         */
        reference operator*() const 
        {
            return *ptr;
        }       
        
        /**
         * Operatore freccia. 
         * @return il puntatore al dato "puntato" dall'iteratore.
         */
        pointer operator->() const 
        {
            return ptr;
        }
    
        /**
         * Operatore di confronto (uguaglianza).
         * @param other const_iteratore da confrontare
         * @return true se i due iteratori "puntano" allo stesso dato
         */
        bool operator==(const const_iterator &other) const 
        {
            return ptr == other.ptr;
        }
        
        /**
         * Operatore di confronto (disuguaglianza). 
         * @param other const_iteratore da confrontare
         * @return true se i due iteratori "puntano" a dati diversi
         */
        bool operator!=(const const_iterator &other) const 
        {
            return !(*this == other);
        }

        /**
         * Operatore di "elemento successivo". 
         * Versione pre-incremento. 
         * @return l'iteratore che "punta" all'elemento successivo
         */
        const_iterator &operator++() 
        {
            ++ptr;
            return *this;
        }
        
        /**
         * Operatore di "elemento successivo". 
         * Versione post-incremento. 
         * @return l'iteratore che "punta" all'elemento successivo
         */
        const_iterator operator++(int) 
        {
            const_iterator tmp(ptr);
            ++ptr;
            return tmp;
        }
  
    private:
            
        const value_type *ptr;
        
        /**
         * Costruttore di inizializzazione. Sarà usato anche da Matrice.
         * @param p puntatore ai dati 
         */
        explicit const_iterator(const value_type *p) : ptr(p)  {}    

    };

    //=========================================================================
    //      MATRIX - FUNCTION ITERATOR
    //=========================================================================

    iterator begin() 
    {
        return iterator(_buffer);
    }
  
    iterator end() 
    {
        return iterator(_buffer + (_rows * _cols));
    }
  
    const_iterator begin() const 
    {
        return const_iterator(_buffer);
    }
  
    const_iterator end() const 
    {
        return const_iterator(_buffer + (_rows * _cols));
    }
};

//=============================================================================
//      MATRIX - FUNTORE DI VERIFICA
//=============================================================================

/**
 * Funtore per confronto fra valori T in input.
 * @param valA Valore sinistro da confrontare
 * @param valB Valore destro da confrontare
 * @return Valore di verità (true/false) del confronto
 */

template <typename T>
struct checkSame
{
    inline bool operator()(const T valA, const T valB) const 
    {
        return (valA == valB);
    }
};

//=============================================================================
//      MATRIX - FUNZIONE CHIAMANTE A FUNTORE
//=============================================================================

/**
 * Funzione globale per esecuzione di funtori fra Matrici<T>.
 * @param M1 Matrice di confronto di valori T
 * @param M2 Matrice di confronto di valori T
 * @param functor Funzione (Funtore<T>) da eseguire per ogni valore T
 * @return Reference allo stream di output (permette la concatenazione)
 */
template <typename T, typename F>
bool verify(const Matrice<T> &M1, const Matrice<T> &M2, const F &functor)
{
    if(M1.getCols() != M2.getCols() ||
       M1.getRows() != M2.getRows())
        return false;

    try
    {
        for (typename Matrice<T>::index_type i = 0; i < M1.getRows(); i++)
            for (typename Matrice<T>::index_type n = 0; n < M1.getCols(); n++)
                if(!functor(M1.getValue(i, n), M2.getValue(i, n)))
                    return false;
    }
    catch(char* str)
    {
        std::cout << str << std::endl;
        throw str;
        return false;
    }

    return true;
}

//=============================================================================
//      MATRIX - OPERATORE DI STAMPA
//=============================================================================

/**
 * Operatore di scrittura su ostream della Matrice in forma lineare.
 * @param out Stream di output (o qualunque oggetto derivato da ostream)
 * @param mtr Matrice da scrivere in forma lineare
 * @return Reference allo stream di output (permette la concatenazione)
 */
template <typename T>
std::ostream &operator<<(std::ostream &out, const Matrice<T> &mtr) 
{    
    out << "[ ";
    for (typename Matrice<T>::index_type i = 0; i < (mtr.getRows() * mtr.getCols()); ++i)
            out << mtr.getIndex(i) << " ";
    out << "]";

    return out;
}

#endif

